import React from "react"
import AddAndDisplay from "./AddAndDisplay";
import FilteredSearch from "./FilteredSeach";



class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            items:[],
            currentItem:{
                text:'',
                key:''
            },
            filterText:[]
            
         
        }
        this.handleinput=this.handleinput.bind(this)
        this.addItem=this.addItem.bind(this)
        this.deleteItem=this.deleteItem.bind(this)
        this.setUpdate=this.setUpdate.bind(this)
        this.searchItem=this.searchItem.bind(this)
       
    }
    handleinput(event){
        this.setState(
            { 
                currentItem:{
                    text:event.target.value,
                    key:Date.now()
                }
            }
          
        )
    }
    addItem(event){
       
        event.preventDefault();
        let newItem=this.state.currentItem;
        if(newItem!==""){
            const newItems=[...this.state.items,newItem]
            this.setState(
                {
                    items:newItems,
                    currentItem:{
                        text:'',
                        key:''
                    }

                }
            )
        }
    }
    deleteItem(key){
        const filteredItems= this.state.items.filter(item=>item.key!==key)
        this.setState({items:filteredItems})
    }

    setUpdate(text, key){
        const items=this.state.items;
        items.map(item=>{
            if(item.key===key){
                item.text=text
            }
        })
        this.setState({items:items})
    }

    searchItem(event){
        const filteredItems=this.state.items.filter(item=>item.text===event.target.value)
        this.setState({filterText:filteredItems})
        
    }
  

  
    render(){
      
          return (
            <main className="App">
            <AddAndDisplay addItem={this.addItem} currentItem={this.state.currentItem.text}
                    handleinput={this.handleinput} items={this.state.items} deleteItem={this.deleteItem}
                    setUpdate={this.setUpdate}/>
             <FilteredSearch searchItem={this.searchItem} filterText={this.state.filterText}/> 
             
               </main>
          )
      
            

                
            
             

           
        
    }
}

export default App