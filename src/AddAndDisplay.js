import React from "react"
import ListItems from "./ListItems";


function AddAndDisplay(props){
    return(
        <div>
        <form id="to-do-form" onSubmit={props.addItem} >
            <input type="text" 
                  placeholder="Enter text" 
                  value={props.currentItem}
                  onChange={props.handleinput}
                  className="AddDisplay"
              ></input>
            <button>Submit</button>
        </form>
       
        <ListItems items={props.items}
                    deleteItem={props.deleteItem}
                    setUpdate={props.setUpdate}/><br></br>
        </div>
    )
}
export default AddAndDisplay