import React from "react"

function ListItems(props){
    const items=props.items;
    const listitems=items.map(item=>{
        return <div className="list" key={item.key}>
                <p>
                    <input type="text" 
                        id={item.key} 
                        value={item.text}
                        onChange={(event)=>{
                            props.setUpdate(event.target.value, item.key)
                        }}
                    ></input>
                    <button onClick={()=>{props.deleteItem(item.key)}}>Delete</button>
                </p>
               
            </div>
    })
    return(
        <div>
            {listitems}
        </div>
    )
}

export default ListItems